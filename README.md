# Archive repository
**!!! This repository is obsolete and no longer maintained !!!**

**This repository's features have been move to the gateware repository: https://git.beagleboard.org/beaglev-fire/gateware**



# BeagleV Fire Bitstream Builder

## Introduction
The BeagleV Fire bitstream builder is a Python script that builds both the PolarFire SoC HSS bootloader and Libero FPGA project into a single programming bitstream. It uses a list of repositories/branches specifying the configuration of the BeagleV Fire to build.


## Prerequisites
### Python libraries
The following Python libraries are used:
- GitPython
- PyYAML

```
pip3 install gitpython
pip3 install pyyaml
```

### Microchip Tools
The SoftConsole and Libero tools from Microchip are required by the bitstream builder.

The following environment variables are required for the bitstream builder to use the Microchip tools:
- SC_INSTALL_DIR
- FPGENPROG
- LIBERO_INSTALL_DIR
- LM_LICENSE_FILE

An example script for setting up the environment is available [here](https://git.beagleboard.org/beaglev-fire/Microchip-FPGA-Tools-Setup). 

## Usage

```
python3 bvf-bitstream-builder.py <YAML Configuration File>
```

For example, the following command will be build the default beagleV Fire configuration:
```
python3 bvf-bitstream-builder.py ./build-options/release/default.yaml
```


### YAML Configuration Files
The YAML configuration files are located in the "build-options" directory.

| Configuration File | Description                                                |
| ------------------ | ---------------------------------------------------------- |
| default.yaml       | Default gateware including default cape and M.2 interface. |
| minimal.yaml       | Minimal Linux system including Ethernet. No FPGA gateware. |

## Supported Platforms
The BeagleV Fire bitstream builder has been tested on Ubuntu 20.04.
